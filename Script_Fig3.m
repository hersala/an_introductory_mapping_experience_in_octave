%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script creates a figure of the Malvinas Islands,
% the Scotia Arc and the Antarctic Peninsula
% 
% Author: Hernan E. Sala, Instituto Antartico Argentino (IAA-DNA)
% The script was tested with M_Map v1.4i and v1.4j (2017/18 � R. Pawlowicz) using
% GNU Octave 4.2.0 running on � Windows XP Professional 2002, Pack 3 and Windows 7
% A very similar script was also tested in M_Map v1.4h (updated Sep/2014).
% It is licensed free of charge and can be used under a MIT license. Even when
% it has been done conscientiously there is no kind of warranty about it.
%
% In order to run this script it is necessary to previously perform:
% 1.Download M_Map package (R. Pawlowicz) https://www.eoas.ubc.ca/~rich/map.html
% 2.Unpack and install M_Map in a directory that can be called from Octave.
% 3.Download, unzip and save in a known directory the following DEMs files:
% 
% 3.1 "tbase" is a 5-minute DEM: TerrainBase (NGDC/NESDIS/NOAA, 1995), file: 'tbase.z',
%     (https://rda.ucar.edu/datasets/ds759.2/). Then create the file 'tbase.int'.
%     See how to do it using "m_tba2b.m" in "Users Guide v1.4" of M_map.
%     The "tbase" DEM is obsolete and should be considered only for code learning
%
% 3.2 "ETOPO2" is 2-minute Global Relief Data (NOAA/NESDIS/NCEI, 2006), 
%     file: 'ETOPO2v2g_i2_MSB.bin',
%     available at: https://www.ngdc.noaa.gov/mgg/global/etopo2.html         
%
% 3.3 "ETOPO1" is a 1-minute Global Relief Model (NOAA/NESDIS/NCEI, 2008), 
%     file: 'etopo1_ice_g_i2.bin', available at: https://goo.gl/JXdyof
%
% 4. In order to use any of the 3 different DEMs (tbase, etopo2v2, etopo1),
%    modify the following functions of M_Map:
%
%  - On m_tbase.m function edit the path according to your current directory 
%    (see lines 29-33)
%    
%  - From m_etopo2.m create and save a new file called m_etopo1.m, then:
%    - edit the 1st line replacing "m_etopo2" by "m_etopo1" (in order to avoid
%      a warning)
%    - in line 40 edit the path according to your current directory.
%    - lines 52-54: comment (%) or not, depending on your selection, e.g.:
%      uncomment line 54: efid=fopen([PATHNAME 'etopo1_ice_g_i2.bin'],'r','l');
%      and comment lines 52 and 53 (add "%" at its beginning).
%    - edit line 60: resolution=1;  % 2 = 2 minute (etopo2), 1 = 1 minute (etopo1) 
%
%  - On m_etopo2.m function edit: 
%    - the path according to your current directory (lines 37-41).
%    - lines 52-54: comment (%) or not, depending on your selection, e.g.:
%      uncomment line 52: efid=fopen([PATHNAME 'ETOPO2v2g_i2_MSB.bin'],'r','b');
%      and comment lines 53 and 54 (add "%" at its beginning).  
%    - line 60: resolution=2;  % 2 = 2 minute (etopo2), 1 = 1 minute (etopo1)
%
% 5. Run the script and once the output figure is finished, maximize
%    the graphics toolkit (figure window) to properly see the map.
%
% 6. Enjoy (?)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; % Clear all local and global user-defined variables.
figure (); % Opens a new graphics window (the default graphics toolkit).
axis ('off'); % Activate axis or not (on/off)

% Defines the projection and the area to be mapped
m_proj ('lambert', 'lon', [-70 -24], 'lat', [-65 -51]);

% Select ONE of the 3 following functions (internally linked to the corresponding
% DEMs file) and comment the other 2. Altimetric resolution is plotted
% using: [min: step: max], 'contourf' creates filled contours
%m_tbase ('contourf',[-9000:1000:2000], 'linestyle', ':', 'linewidth',0.1);
%m_etopo2 ('contourf',[-9000:1000:2000], 'linestyle', ':', 'linewidth',0.1);
m_etopo1 ('contourf',[-9000:1000:2000], 'linestyle', ':', 'linewidth',0.1);
%   Altimetric resolution is plotted in figure using: [min: step: max]
%   'contourf' creates filled contours

FS= 22; % FS is the fontsize of the labels

% Create a grid on the DEM map
m_grid ('tickdir','out','linewidth',1,'fontsize',0.7*FS, 'linestyle', ':');
colorbar ('fontsize',0.75*FS, "SouthOutside") % Places a colorbar under the map

colormap ("winter") % Octave has many built-in colormaps: viridis (default), etc.

% Title properties
title ("Malvinas Islands, Scotia Arc and Antarctic Peninsula", 'fontsize',1.1*FS, 'fontweight','bold');

% Some geographical references labels

[X1,Y1]=m_ll2xy(-44.3,-64.5); %  Converts lon/lat to map coordinates
text(X1,Y1,'Weddell Sea', 'fontsize',FS, 'color','red','fontweight','bold');

[X2,Y2]=m_ll2xy(-49,-57.5); %  Converts lon/lat to map coordinates
text(X2,Y2,"Scotia \n  Sea", 'fontsize',FS, 'color','red','fontweight','bold');

[X3,Y3]=m_ll2xy(-64,-52.3); %  Converts lon/lat to map coordinates
text(X3,Y3,"Malvinas Islands", 'fontsize',FS, 'color','red','fontweight','bold');

[X4,Y4]=m_ll2xy(-34,-55.6); %  Converts lon/lat to map coordinates
text(X4,Y4,"Scotia\n  Arc", 'fontsize',FS, 'color','red','fontweight','bold');

[X6,Y6]=m_ll2xy(-66.8,-61.2); %  Converts lon/lat to map coordinates
text(X6,Y6,"  South\nShetland \n Islands", 'fontsize',FS, 'color','red','fontweight','bold');

% Some permanent stations in the area
MS= 7; % MS is the markersize to signal the location of the stations

[X10,Y10]=m_ll2xy(-58.7,-62.2); % Carlini Station (lon, lat)
line(X10,Y10,'marker','square','markersize',MS,'markeredgecolor', 'red', 'markerfacecolor', 'yellow');

[X11,Y11]= m_ll2xy(-56.98,-63.4); % Espezanza Station (lon, lat)
line(X11,Y11,'marker','square','markersize',MS,'markeredgecolor', 'red', 'markerfacecolor', 'yellow');

[X12,Y12]= m_ll2xy(-56.63,-64.23); % Marambio Station (lon, lat)
line(X12,Y12,'marker','square','markersize',MS,'markeredgecolor', 'red', 'markerfacecolor', 'yellow');

[X13,Y13]= m_ll2xy(-44.73,-60.73); % Orcadas Station (lon, lat)
line(X13,Y13,'marker','square','markersize',MS,'markeredgecolor', 'red', 'markerfacecolor', 'yellow');
