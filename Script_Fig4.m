%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script creates a two panels (L&R) of the Antarctic Peninsula and the 
% Weddell Sea region, including some features present there.
% 
% Author: Hernan E. Sala, Instituto Antartico Argentino (IAA-DNA)
% The script was tested with M_Map v1.4i and v1.4j (2017/18 ® R. Pawlowicz) using
% GNU Octave 4.2.0 running on ® Windows XP Professional 2002, Pack 3 and Windows 7
% A very similar script was also tested in M_Map v1.4h (updated Sep/2014).
% It is licensed free of charge and can be used under a MIT license. Even when
% it has been done conscientiously there is no kind of warranty about it.
%
% In order to run this script it is necessary to previously perform:
% 1.Download M_Map package (R. Pawlowicz) https://www.eoas.ubc.ca/~rich/map.html
% 2.Unpack and install M_Map in a directory that can be called from Octave.
% 3.Download, unzip and save in a known directory the following DEMs files:
% 
% 3.1 "tbase" is a 5-minute DEM: TerrainBase (NGDC/NESDIS/NOAA, 1995), file: 'tbase.z',
%     (https://rda.ucar.edu/datasets/ds759.2/). Then create the file 'tbase.int'.
%     See how to do it using "m_tba2b.m" in "Users Guide v1.4" of M_map.
%     The "tbase" DEM is obsolete and should be considered only for code learning
%
% 3.2 "ETOPO2" is 2-minute Global Relief Data (NOAA/NESDIS/NCEI, 2006), 
%     file: 'ETOPO2v2g_i2_MSB.bin',
%     available at: https://www.ngdc.noaa.gov/mgg/global/etopo2.html         
%
% 3.3 "ETOPO1" is a 1-minute Global Relief Model (NOAA/NESDIS/NCEI, 2008), 
%     file: 'etopo1_ice_g_i2.bin', available at: https://goo.gl/JXdyof
%
% 4. In order to use any of the 3 different DEMs (tbase, etopo2v2, etopo1),
%    modify the following functions of M_Map:
%
%  - On m_tbase.m function edit the path according to your current directory 
%    (see lines 29-33)
%    
%  - From m_etopo2.m create and save a new file called m_etopo1.m, then:
%    - edit the 1st line replacing "m_etopo2" by "m_etopo1" (in order to avoid
%      a warning)
%    - in line 40 edit the path according to your current directory.
%    - lines 52-54: comment (%) or not, depending on your selection, e.g.:
%      uncomment line 54: efid=fopen([PATHNAME 'etopo1_ice_g_i2.bin'],'r','l');
%      and comment lines 52 and 53 (add "%" at its beginning).
%    - edit line 60: resolution=1;  % 2 = 2 minute (etopo2), 1 = 1 minute (etopo1) 
%
%  - On m_etopo2.m function edit: 
%    - the path according to your current directory (lines 37-41).
%    - lines 52-54: comment (%) or not, depending on your selection, e.g.:
%      uncomment line 52: efid=fopen([PATHNAME 'ETOPO2v2g_i2_MSB.bin'],'r','b');
%      and comment lines 53 and 54 (add "%" at its beginning).  
%    - line 60: resolution=2;  % 2 = 2 minute (etopo2), 1 = 1 minute (etopo1)
%
% 5. Run the script and once the output figure is finished, maximize
%    the graphics toolkit (figure window) to properly see the map.
%
% 6. Enjoy (?)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear; % All user-defined variables are cleared.
figure (); % Opens a new graphics window (using the default graphics toolkit).
cmap = summer (64); % Creates 64-color palette that is used on both panels
FS= 20; % FS is the markersize and the fontsize used in labels, markers and titles

%%%%%%%%%% Left panel %%%%%%%%%%
subplot (1, 2, 1); % Sets that this is the first graph of two graphs in columns
axis ('off'); % Activates axis or not (on/off)

% Defines the projection and the area to be mapped
m_proj ('lambert', 'lon', [-66 -54], 'lat', [-69 -61]); 

%  Altimetric resolution is plotted in figure using: [min: step: max]
%  'contourf' creates filled contours
m_etopo2 ('contourf',[-8000:1000:2000], 'linestyle', '-', 'linewidth',0.1);

% Creates a grid on the DEM.
m_grid ('linestyle','-','tickdir','in','linewidth',1.3, 'fontsize',12); 
colormap (cmap); % There are many built-in colormaps in Octave: viridis (default), etc.
colorbar ('fontsize',0.6*FS,"SouthOutside"); % Plots a colorbar

% Positions and properties of labels (coordinates are in lon, lat.)
m_text(-64, -61.3,"Drake Passage",'color','blue','fontsize',FS);
m_text(-64.4, -67.1,"   Larsen\n Ice Shelf",'color','blue','fontsize',FS);
m_text(-59.6, -66.2,"Weddell Sea",'color','blue','fontsize',FS);

% Properties of the title
title ("Data from ETOPO2v2, NCEI-NOAA (2006)", 'fontweight','bold','fontsize',1.1*FS);

%%% Matienzo Station
[X1,Y1]=m_ll2xy(-60.07,-64.98); %  Converts lon/lat to map coordinates
line(X1,Y1,'marker','square','markersize',FS/3,'markeredgecolor', 'black', 'markerfacecolor', 'red');
text(X1,Y1,'   Matienzo St.', 'fontsize',FS-4, 'color','black', 'fontweight','bold');

%%%%%%%%%% Right panel %%%%%%%%%%
subplot (1, 2, 2); % Sets that this is the second graph of two graphs in columns
axis ('off'); % Activates axis or not (on/off)

% Define the projection and the area to be mapped
m_proj ('lambert', 'lon', [-66 -54], 'lat', [-69 -61]);

m_etopo1 ('contourf',[-8000:1000:2000], 'linestyle', '-', 'linewidth',0.1);
%  Altimetric resolution is plotted in figure using: [min, step, max]
%  'contourf' creates filled contours

% Creates a grid on the DEM.
m_grid ('linestyle','-','tickdir','in','linewidth',1.3, 'fontsize',12); 
colormap (cmap); % There are many built-in colormaps in Octave: viridis (default), etc.
colorbar ('fontsize',0.6*FS,"SouthOutside"); % Plots a colorbar. 

% Positions and properties of labels (coordinates are in lon, lat.)
m_text(-64, -61.3,"Drake Passage",'color','blue','fontsize',FS);
m_text(-64.4, -67.1,"   Larsen\n Ice Shelf",'color','blue','fontsize',FS);
m_text(-59.6, -66.2,"Weddell Sea",'color','blue','fontsize',FS);

% Properties of the title
title ("Data from ETOPO1, NOAA-CIRES (2009)", 'fontweight','bold','fontsize',1.1*FS);

%%% Matienzo Station
line(X1,Y1,'marker','square','markersize',FS/3,'markeredgecolor', 'black', 'markerfacecolor', 'red');
text(X1,Y1,'   Matienzo St.', 'fontsize',FS-4, 'color','black', 'fontweight','bold');

% Legend included in the lower part of the right panel (the coordinates are in lon, lat.)
m_text(-64.9, -68.4,"Notice the differences in the extension of\n  the Larsen Ice Shelf and the different \
\n     spatial resolution of the two DEMs",'color','black','fontsize',0.56*FS);
