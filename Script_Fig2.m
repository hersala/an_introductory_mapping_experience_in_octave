%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script creates a figure of the Antarctic Peninsula and the Weddell Sea
% region, including some of the permanent stations present there.
% 
% Author: Hernan E. Sala, Instituto Antartico Argentino (IAA-DNA)
% The script was tested with M_Map v1.4i and v1.4j (2017/18 � R. Pawlowicz) using
% GNU Octave 4.2.0 running on � Windows XP Professional 2002, Pack 3 and Windows 7
% (a very similar script was also tested in M_Map v1.4h, updated Sep/2014).
% It is licensed free of charge and can be used under a MIT license. Even when
% it has been done conscientiously there is no kind of warranty about it.
%
% In order to run this script it is necessary to previously perform:
% 1.Download M_Map package (� R. Pawlowicz) https://www.eoas.ubc.ca/~rich/map.html
% 2.Unpack and install M_Map in a directory that can be called from Octave.
% 3.Download, unzip and save in a known directory the following DEMs files:
% 
% 3.1 "tbase" is a 5-minute DEM: TerrainBase (NGDC/NESDIS/NOAA 1995), file: 'tbase.z',
%     (https://rda.ucar.edu/datasets/ds759.2/). Then create the file 'tbase.int',
%     see how to do it using "m_tba2b.m" in "Users Guide v1.4" of M_map
%     The "tbase" DEM is obsolete and should be considered only for code learning
%
% 3.2 "ETOPO2" is 2-minute Global Relief Data (NOAA/NESDIS/NCEI, 2006),
%     file: 'ETOPO2v2g_i2_MSB.bin', 
%     available at: https://www.ngdc.noaa.gov/mgg/global/etopo2.html         
%
% 3.3 "ETOPO1" is a 1-minute Global Relief Model (NOAA/NESDIS/NCEI, 2008), 
%     file: 'etopo1_ice_g_i2.bin', available at: https://goo.gl/JXdyof
%
% 4. In order to use any of the 3 different DEMs (tbase, etopo2v2, etopo1),
%    modify the following functions of M_Map:
%
%  - On m_tbase.m function edit the path according to your current directory
%    (see lines 29-33)
%
%  - From m_etopo2.m create and save a new file called m_etopo1.m:
%    Then edit the 1st line replacing "m_etopo2" by "m_etopo1" (in order to avoid
%    a waning). In line 40 edit the path according to your current directory.
%
%  - On m_etopo2.m function edit: 
%    - the path according to your current directory (lines 37-41 in m_etopo2.m).
%    - lines 52 and 54: comment (%) or not, depending on your selection, e.g.:
%       uncomment line 52: efid=fopen([PATHNAME 'ETOPO2v2g_i2_MSB.bin'],'r','b');
%       and comment line 54 (add an % at its beginning).  
%    - line 60: resolution=2;  % 2 = 2 minute (etopo2), 1 = 1 minute (etopo1)
%
% 5. Run the script and once the output figure is finished, maximize
%    the graphics toolkit (figure window) to properly see the map.
%
% 6. Enjoy (?)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; % Clear all local and global user-defined variables.
figure (); % Opens a new graphics window (the default graphics toolkit).
axis ('off'); % Activate axis or not (on/off)

% Defines the projection and the area to be mapped
m_proj ('lambert', 'lon', [-75 -25], 'lat', [-90 -58], 'par', [-80 -70], 'ell', 'wgs84'); 

% Select ONE of the 3 following functions (internally linked with the corresponding
% DEM file), and comment the other 2 (add "%"). 'contourf' creates filled contours
%m_tbase ('contourf', 'linestyle', 'none');
m_etopo2 ('contourf', 'linestyle', 'none'); 
%m_etopo1 ('contourf', 'linestyle', 'none');

MS= 9; % MS is the markersize for the placement of the stations
FS= 29; % FS is the fontsize for the title and the labels of the stations
TNR='Times New Roman'; % TNR it is used to set the FontName

% Creates a grid on the DEM.
m_grid ('linestyle','-','tickdir','in','linewidth',1.3, 'fontname', TNR, 'fontsize',0.8*FS); 

% Places a scale color bar
colorbar ('fontsize',0.75*FS, 'FontName', TNR, "EastOutside"); 
colormap ("viridis"); % There are many possible options: viridis (default), etc.

% Sets the title and its properties
title ("Antarctic Peninsula and Weddell Sea Region", 'fontsize', 1.2*FS, 'fontweight','bold', 'FontName', TNR); 

% Sets the content and position of a label informing the propierties of the sketch
text (-1.55e+06, -1.5e+06, "Proj.: Lambert Conf. Conic\nStd. parallels: 70�S and 80�S" ...
, 'fontsize',0.77*FS, 'FontName', TNR);
% "\n" creates a new line when it is between brackets.
% ... is a continuation line marker

% Plot Argentinian permanent stations in the Antarctic Peninsula and Weddell Sea Region
%  m_ll2xy converts long,lat to X,Y coordinates using the current projection
[X2,Y2]= m_ll2xy(-56.98,-63.4); 
line(X2,Y2,'marker','diamond','markersize',MS, 'markeredgecolor', 'black', 'markerfacecolor', 'red');
text(X2,Y2+20000,'  Esperanza St.', 'fontsize', FS, 'color','red', 'fontweight','normal', 'FontName', TNR);

[X3,Y3]= m_ll2xy(-44.73,-60.73);
line(X3,Y3,'marker','diamond','markersize',MS, 'markeredgecolor', 'black', 'markerfacecolor', 'red');
text(X3-70000,Y3+93000,'Orcadas St.', 'fontsize', FS, 'color','red', 'fontweight','normal', 'FontName', TNR);

[X4,Y4]= m_ll2xy(-56.63,-64.23);
line(X4,Y4,'marker','diamond','markersize',MS, 'markeredgecolor', 'black', 'markerfacecolor', 'red');
text(X4-210000,Y4-83000,'Marambio St.', 'fontsize',FS, 'color','red', 'fontweight','normal', 'FontName', TNR);

[X5,Y5]= m_ll2xy(-67.1,-68.13);
line(X5,Y5,'marker','diamond','markersize',MS, 'markeredgecolor', 'black', 'markerfacecolor', 'red');
text(X5,Y5,'  San Martin St.', 'fontsize',FS, 'color','red', 'fontweight','normal', 'FontName', TNR);

[X6,Y6]= m_ll2xy(-34.62,-77.8);
line(X6,Y6,'marker','diamond','markersize',MS, 'markeredgecolor', 'black', 'markerfacecolor', 'red');
text(X6-550000,Y6+99000,'Belgrano II St.', 'fontsize',FS, 'color','red', 'fontweight','normal', 'FontName', TNR);

[X6,Y6]= m_ll2xy(-58.66,-62.23);
line(X6,Y6,'marker','diamond','markersize',MS, 'markeredgecolor', 'black', 'markerfacecolor', 'red');
text(X6,Y6+78000,' Carlini St.', 'fontsize',FS, 'color','red', 'fontweight','normal', 'FontName', TNR);

% Position and properties of the labels
[X7,Y7]= m_ll2xy(-39.4,-68); % Weddell Sea label
text(X7,Y7,"Weddell \n   Sea", 'fontsize',1.1*FS, 'color','black', 'FontName', TNR);

[X7,Y7]= m_ll2xy(-69.6,-59.1); % Drake Passage label
text(X7,Y7,"  Drake \n Passage", 'fontsize',1.1*FS, 'color','black', 'FontName', TNR);
