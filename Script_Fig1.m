%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script creates a figure containing three sketches of the southern ocean 
% basins that surround the Antarctic continent.
%
% Author: Hernan E. Sala, Instituto Antartico Argentino (IAA-DNA)
% The script was tested with M_Map v1.4i and v1.4j (® R. Pawlowicz) using
% GNU Octave 4.2.0 running on ® Windows XP Professional 2002, Pack 3 and Windows 7
% A very similar script was also tested in M_Map v1.4h (updated Sep/2014).
% It is licensed free of charge and can be used under a MIT license. Even when
% it has been done conscientiously there is no kind of warranty about it.
%
% In order to run this script it is necessary to previously perform:
% 1.Download M_Map package (® R. Pawlowicz) https://www.eoas.ubc.ca/~rich/map.html
% 2.Unpack and install M_Map in a directory that can be called from Octave.
% 3. Run the script and once the output figure is finished, maximize
%    the graphics toolkit (figure window) to properly see the map.
% 4. Enjoy (?)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; % Clear all local and global user-defined variables.
figure (); % Opens a new graphics window (the default graphics toolkit).
axis ('off'); % Activate axis or not (on/off)

FS= 18; % FS is the fontsize for the title and the labels of the stations

% Set customized colors (choose the ones you prefer)
colour_teal = [18 150 155] ./ 255;
colour_peach = [251 111 66] ./ 255;
% colour_lightgreen = [94 250 81] ./ 255;
% colour_green = [12 195 82] ./ 255;
% colour_lightblue = [8 180 238] ./ 255;

%%%%%%%%%% Left panel %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot (1, 3, 1); % Sets that this is the first sketch of one row
% Defines the projection and some map properties
m_proj('Ortho','lat',-58,'long',-140');
m_coast('patch', colour_peach);

% Creates a grid
m_grid('linest','-','xticklabels',[],'yticklabels',[],...
 'backgroundcolor', colour_teal, 'xtick', 6, 'ytick', 4);
 
% Sets the title and its properties
title ("South Pacific Ocean Basin", 'fontsize', 1.1*FS); 

%%%%%%%%%% Middle panel %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot (1, 3, 2); % Sets that this is the second sketch of one row
% Defines the projection and some map properties
m_proj('Ortho','lat',-57,'long',-20');
m_coast('patch', colour_peach);

% Creates a grid
m_grid('linest','-','xticklabels',[],'yticklabels',[],...
 'backgroundcolor', colour_teal, 'xtick', 6, 'ytick', 4);
 
% Sets the title and its properties
title ("South Atlantic Ocean Basin", 'fontsize', 1.1*FS); 

%%%%%%%%%% Right panel %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot (1, 3, 3); % Sets that this is the third sketch of one row
% Defines the projection and some map properties
m_proj('Ortho','lat',-56,'long',80');
m_coast('patch', colour_peach);

% Creates a grid
m_grid('linest','-','xticklabels',[],'yticklabels',[],...
 'backgroundcolor', colour_teal, 'xtick', 6, 'ytick', 4);
 
% Sets the title and its properties
title ("South Indian Ocean Basin", 'fontsize', 1.1*FS); 
