# An introductory mapping experience in Octave

[EN] Here you will find a set of scripts that can be used as examples for an introductory mapping experience in [GNU Octave](https://www.gnu.org/software/octave/). They were created and tested using the [M_Map](https://www.eoas.ubc.ca/~rich/map.html) package (versions v1.4i and v1.4j, 2017/18 © R. Pawlowicz) using GNU Octave 4.2.0 running on MS Windows XP Professional 2002 (Pack 3) and Windows 7 ©. Very similar scripts were also successfully tested in M_Map v1.4h (2014).

# Una experiencia de mapeo introductoria en Octave

[ES] Aquí se encuentran disponibles un conjunto de scripts que pueden ser utilizados a modo de ejemplo para iniciarse en la elaboración de mapas con [GNU Octave](https://www.gnu.org/software/octave/). Estos fueron creados y testeados utilizando el paquete [M_Map](https://www.eoas.ubc.ca/~rich/map.html) (versiones v1.4i y v1.4j, 2017/18 © R. Pawlowicz), en GNU Octave 4.2.0, en un entorno MS Windows XP Professional 2002 (Pack 3) y Windows 7 ©. Algunos scripts muy similares también fueron testeados con éxito en M_Map v1.4h (2014).

